import java.util.ArrayList;
import java.util.Random;

public class Deck {
    private ArrayList<String> cards;
    private static Random random = new Random();

    public Deck(){
        cards = new ArrayList<String>();
    }

    public void initializeGameDeck(){
        String[] cardValues = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
        String[] cardTypes = { "H", "D", "S", "C" };

        // Nested for loop >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        for (String type : cardTypes){
            for(String cardValue : cardValues){
                cards.add(cardValue + type);
            }
        }
    }

    public void shuffle(){
        for(int i=0; i < cards.size(); i++){
            
            int randomNum = random.nextInt(52);

            String thisCard = cards.get(i);
            String randomCard = cards.get(randomNum);
            
            cards.set(i, randomCard);
            cards.set(randomNum, thisCard);
        }
        Test.isThereDupe(cards);         // Running tests from here ################# ???
    }

    public void deal(Player player1, Player player2){
        for(int i=0; i < cards.size(); i++){
            if(i % 2 == 0){
                ArrayList<String> player1Cards = player1.getPlayDeck().getCards();
                player1Cards.add(cards.get(i));
                player1.setPlayDeck(player1Cards);
            } else {
                ArrayList<String> player2Cards = player2.getPlayDeck().getCards();
                player2Cards.add(cards.get(i));
                player2.setPlayDeck(player2Cards);
            }
        }
        System.out.println("player 1 cards:");
        System.out.println(player1.getPlayDeck().getCards().toString() + "/n");
        System.out.println("player 2 cards:");
        System.out.println(player2.getPlayDeck().getCards().toString());
    }

    public ArrayList<String> getCards(){
        return cards;
    }

    public void setCards(ArrayList<String> cards){
        this.cards = cards;
    }

    public ArrayList<String> getPlayCards(){
        return cards;
    }

}