import java.util.HashMap;
import java.util.ArrayList;

public class Game {
    private Boolean gameOver;
    HashMap<String, ArrayList<String>> playedCardsOnTable;
    Deck initialGameDeck = new Deck();
    Player player1;
    Player player2;

    public Game(){
        gameOver = false;
        initialGameDeck = new Deck();
        playedCardsOnTable = new HashMap<String, ArrayList<String>>();
        player1 = new Player();
        player2 = new Player();
    }

    public void setup(){
        initialGameDeck.initializeGameDeck();
        initialGameDeck.shuffle();
        initialGameDeck.deal(player1, player2);
    }

    public void flip(){
        String player1card = player1.playCard();
        String player2card = player2.playCard();
    }

    public void evaluateFlip(){
    }

    public Player getWinner(){
        return new Player();
    }

    public void play(){
        setup();
    }

}