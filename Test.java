import java.util.ArrayList;
import java.util.Arrays;

class Test {

    public static void gameCreatesTwoPlayers(){
        Game game = new Game();
        if(game.player1 != null){
            System.out.println("$$$$$ Test PASS >> player1 exists");
        } else{
            System.out.println("$$$$$ ERROR: player1 is null");
        }
        if(game.player2 != null){
            System.out.println("$$$$$ Test PASS >> player2 exists");
        } else{
            System.out.println("$$$$$ ERROR: player2 is null");
        }
    }

    public static void deckInitializes52Cards(){
        Deck deck = new Deck();
        deck.initializeGameDeck();
        if(deck.getCards().size() == 52){
            System.out.println("$$$$$ Test PASS >> Initialize Deck creates an Array with 52 cards");
        }
    }

    public static void deckInitializeCreatesCorrectCards(){
        Deck deck = new Deck();
        deck.initializeGameDeck();

        // Why does toString include square brackets??    >>>>>>>>>>>>>>>>>>>>>>>>>>

        String correctCards = "[AH, 2H, 3H, 4H, 5H, 6H, 7H, 8H, 9H, 10H, JH, QH, KH, AD, 2D, 3D," +
                                " 4D, 5D, 6D, 7D, 8D, 9D, 10D, JD, QD, KD, AS, 2S, 3S, 4S, 5S," +  " 6S, 7S, 8S, 9S, 10S, JS, QS, KS, AC, 2C, 3C, 4C, 5C, 6C, 7C," + 
                                " 8C, 9C, 10C, JC, QC, KC]";
        if(deck.getCards().toString().equals(correctCards)){
            System.out.println("$$$$$ Test PASS >> Initialize Deck creates an Array with correct cards");
        } else {
            System.out.println("$$$$$ Test FAIL >> Initialize Deck creates an Array with incorrect cards");
            System.out.println(deck.getCards().toString());
            System.out.println(correctCards);
        }
    }

    public static void initializeGameCreatesArrayOfCards(){
        Game game = new Game();
        game.setup();
        if(game.initialGameDeck.getCards().size() == 52){
            System.out.println("$$$$$ Test PASS >> Game sets up and has 52 cards");
        } else {
            System.out.println("$$$$$ Test FAIL >> Game not setting up correctly with 52 cards");
        }
    }

    public static void shuffleDoesntReturnUnshuffled(){
        Deck deck = new Deck();
        deck.initializeGameDeck();
        String unshuffledCards = deck.getCards().toString();
        deck.shuffle();
        String shuffledCards = deck.getCards().toString();

        if(unshuffledCards.equals(shuffledCards)){
            System.out.println("$$$$$ Test FAIL >> Shuffle method returns unshuffled deck");
        } else {
            System.out.println("$$$$$ Test PASS >> Shuffle method does not return unshuffled deck");
        }
    }

    public static void isThereDupe(ArrayList<String> cards){
        Boolean isDupe = false;
        ArrayList<String> dupes = new ArrayList<String>();
        int count;

        for(int i=0; i < cards.size(); i++){
            count = 0;
            for(int j=0; j < cards.size(); j++){
                
                if(cards.get(i).equals(cards.get(j))){
                    count++;
                    
                    if(count > 1){
                        isDupe = true;
                        dupes.add(cards.get(i));
                        System.out.println("\nHEEYEYYYYYYYY We have DUPE!!!" + cards.get(i) + " ANNNDD" + cards.get(j) + "\n");
                    }
                }
            }
        }
        if(isDupe){
            System.out.println("$$$$$ Test FAIL >> At least one duplicate card exists in this deck");
            System.out.println(cards.toString());
        } else {
            System.out.println("$$$$$ Test Pass >> There are no duplicate cards in this deck");
        }
    }

    public static void shuffleDoesntCreateDupes(){
        Boolean isDupe = false;
        Deck deck = new Deck();
        Deck shuffledDeck = new Deck();
        ArrayList<String> dupes = new ArrayList<String>();
        int count;

        deck.initializeGameDeck();
        ArrayList<String> unshuffledCards = deck.getCards();
        shuffledDeck.initializeGameDeck();

        shuffledDeck.shuffle();
        ArrayList<String> shuffledCards = shuffledDeck.getCards();

        // Better way of doing this?? Rather than nexted loops and nested ifs?   >>>>>>>>>>>>>>>>>

        for(int i=0; i < unshuffledCards.size(); i++){
            count = 0;
            for(int j=0; j < unshuffledCards.size(); j++){                
                if(unshuffledCards.get(i).equals(shuffledCards.get(j))){
                    count++;                    
                    if(count > 1){
                        isDupe = true;
                        dupes.add(unshuffledCards.get(i));
                    }
                }
            }
        }
        if(isDupe){
            System.out.println("\n=======================================\n");
            System.out.println("$$$$$ Test FAIL >> Shuffle method created the following dupes:");
            System.out.println(dupes.toString());
        } else {
            System.out.println("$$$$$ Test PASS >> Shuffle method does not create dupes");
        }
    }

    public static void noCardsMissingFromShuffledDeck(){

        // This ArrayList below makes me compile with: javac War.java -Xlint:unchecked  >>>>>>>>>>>>>>>

        ArrayList<String> cardList = new ArrayList<String>(Arrays.asList("AH", "2H", "3H", "4H", "5H", "6H", "7H", "8H", "9H", "10H", "JH", "QH", "KH", "AD", "2D", "3D", "4D", "5D", "6D", "7D", "8D", "9D", "10D", "JD", "QD", "KD", "AS", "2S", "3S", "4S", "5S", "6S", "7S", "8S", "9S", "10S", "JS", "QS", "KS", "AC", "2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "10C", "JC", "QC", "KC"));

        Deck deck = new Deck();
        deck.initializeGameDeck();
        deck.shuffle();
        ArrayList<String> cards = new ArrayList<String>();
        cards = deck.getCards();
        Boolean noneMissing = true;
        
        for(String card : cards){
            if(!cardList.contains(card)){
                noneMissing = false;
            }
        }
        if(noneMissing){
            System.out.println("$$$$$ Test PASS >> Shuffle method output isn't missing any cards");
        } else {
            System.out.println("$$$$$ Test FAIL >> Shuffled method output is missing cards");
        }
    }

    public static void runTests(){
        gameCreatesTwoPlayers();
        deckInitializes52Cards();
        deckInitializeCreatesCorrectCards();
        initializeGameCreatesArrayOfCards();
        shuffleDoesntReturnUnshuffled();
        shuffleDoesntCreateDupes();
        noCardsMissingFromShuffledDeck();
    }
}