import java.util.ArrayList;

public class Player {
    private Deck playDeck;
    private Deck winDeck;

    public Player(){
        playDeck = new Deck();
        winDeck = new Deck();
    }

    public String playCard(){
        return "";
    }

    public void useWinDeck(){
        winDeck.shuffle();
        playDeck = winDeck;
        //winDeck.cards.clear();
    }

    public Deck getPlayDeck(){
        return playDeck;
    }

    public void setPlayDeck(ArrayList<String> cards){
        this.playDeck.setCards(cards);
    }

    public Deck getWinDeck(){
        return winDeck;
    }

}